resource "aws_s3_bucket" "source" {
  bucket        = "rga-seriatim-mock-source"
  force_destroy = true
  acl           = "private"
  versioning {
    enabled = true
  }
  tags          = var.main_tags

}