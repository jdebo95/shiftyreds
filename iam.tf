##############
# IAM for Redshift
##############
resource "aws_iam_role" "rga_iam_for_redshift" {
  name = "rga_iam_for_redshift"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "redshift.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "rga_redshift_spectrum" {
  name        = "rga_redshift_spectrum"
  path        = "/"
  description = "IAM policy for querying S3 for Redshift"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:Get*",
        "s3:List*"
      ],
      "Resource": "${aws_s3_bucket.source.arn}/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "rga_redshift_s3" {
  role       = aws_iam_role.rga_iam_for_redshift.name
  policy_arn = aws_iam_policy.rga_redshift_spectrum.arn
}

resource "aws_iam_role_policy_attachment" "rga_redshift_glue" {
  role       = aws_iam_role.rga_iam_for_redshift.name
  policy_arn = "arn:aws:iam::aws:policy/AWSGlueConsoleFullAccess"
}


##############
# IAM for GLUE
##############
resource "aws_iam_role" "rga_iam_for_glue" {
  name = "rga_iam_for_glue"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "glue.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "rga_glue_crawler" {
  name        = "rga_glue_crawler"
  path        = "/"
  description = "IAM policy for glue crawler to access S3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:*"
      ],
      "Resource": "${aws_s3_bucket.source.arn}/*"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "rga_glue_s3" {
  role       = aws_iam_role.rga_iam_for_glue.name
  policy_arn = aws_iam_policy.rga_glue_crawler.arn
}