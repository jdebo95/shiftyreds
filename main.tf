variable "main_tags" {
  default     = {
    Name    = "Justin DeBo"
    Manager = "Chris Robison"
    Market  = "St. Louis"
    Project = "RGA-Seriatim"
  }
  description = "Required resource tags"
  type        = map(string)
}

provider "aws" {
  shared_credentials_file = "/Users/justin.debo/.aws/credentials"
  profile                 = "debo-sso"
  region                  = "us-east-2"
}

terraform {
  backend "s3" {
    bucket                  = "jdebo-tfremotestate"
    key                     = "seriatim-redshift.tfstate"
    region                  = "us-east-2"
    shared_credentials_file = "/Users/justin.debo/.aws/credentials"
    profile                 = "debo-sso"
  }
}