resource "aws_glue_catalog_database" "rga_mock_catalog_db" {
  name = "rga_catalog_database"
}

# resource "aws_glue_catalog_table" "rga_mock_catalog_table" {
#   name          = "RGACatalogTable"
#   database_name = aws_glue_catalog_database.rga_mock_catalog_db.name
# }

resource "aws_glue_crawler" "example" {
  database_name = aws_glue_catalog_database.rga_mock_catalog_db.name
  name          = "rga-mock-crawler"
  role          = aws_iam_role.rga_iam_for_glue.arn
  schedule      = "cron(0 1 * * ? *)"
  tags          = var.main_tags

  s3_target {
    path = "s3://${aws_s3_bucket.source.bucket}"
  }
}
