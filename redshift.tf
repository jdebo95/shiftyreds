variable "redshift_user" {}
variable "redshift_pwd" {}

resource "aws_redshift_cluster" "main" {
  cluster_identifier = "rga-mock-redshift"
  database_name      = "get_dunked"
  master_username    = var.redshift_user
  master_password    = var.redshift_pwd
  node_type          = "dc1.large"
  cluster_type       = "single-node"
  tags               = var.main_tags
  skip_final_snapshot = true
}
